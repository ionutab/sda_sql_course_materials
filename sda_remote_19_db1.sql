CREATE SCHEMA `sda_remote_19_db1` ;
use sda_remote_19_db1;

CREATE TABLE IF NOT EXISTS `books` (
    `book_name` VARCHAR(255) NOT NULL,
    `book_authors` VARCHAR(255) UNIQUE
);

CREATE TABLE `authors` (
    `first_name` VARCHAR(255),
    `last_name` VARCHAR(255)
);

alter table `books` drop column book_authors;

SELECT 
    *
FROM
    books;
SELECT 
    *
FROM
    authors;
ALTER TABLE `books` ADD COLUMN `isbn` VARCHAR(30);

drop table authors;
drop table books;

drop schema sda_remote_19_db1;
