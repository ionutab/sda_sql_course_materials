# very basic traditional school schema
-- you can run this sctipt entirely and it might work
CREATE SCHEMA `sda_school_19`;
use `sda_school_19`;
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
	`id` INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` VARCHAR(4) NOT NULL
);
CREATE TABLE `disciplines` (
	`id` INT AUTO_INCREMENT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);
CREATE TABLE `students` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `first_name` VARCHAR(255) NOT NULL,
    `last_name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255),
    `telephone` VARCHAR(12),
    `date_of_birth` DATE NOT NULL,
    `class_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`class_id`)
        REFERENCES `classes` (`id`)
);
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `first_name` VARCHAR(255) NOT NULL,
    `last_name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255),
    `discipline_id` INT NOT NULL UNIQUE,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`discipline_id`)
        REFERENCES `disciplines` (`id`)
);
CREATE TABLE `classes_x_disciplines` (
	`class_id` INT NOT NULL,
    `discipline_id` INT NOT NULL,
    PRIMARY KEY(`class_id`, `discipline_id`),
    FOREIGN KEY (`class_id`)
        REFERENCES `classes` (`id`),
    FOREIGN KEY (`discipline_id`)
        REFERENCES `disciplines` (`id`)
);
drop table if exists `marks`;
CREATE TABLE IF NOT EXISTS `marks` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `student_id` INT NOT NULL,
    `discipline_id` INT NOT NULL,
    `mark` TINYINT NOT NULL,
    `date` DATETIME NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`student_id`)
        REFERENCES `students` (`id`),
    FOREIGN KEY (`discipline_id`)
        REFERENCES `disciplines` (`id`)
);




