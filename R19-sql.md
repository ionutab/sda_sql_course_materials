Movie A, 1988, 8.6
Movie B, 1996, 9.5
Movie C, 2020, 7.4
# we separate data by comma

Movie A| 1988| 8.6
Movie B| 1996| 9.5
Movie C| 2020| 7.4

Movie A|| 1988| 8.6
Movie B|| 1996| 9.5
Movie C|| 2020| 7.4

### Database as in:
RDBMS = Relational Database Management System
Installed Server = Local Installed Server
Schema = A collection of tables and/or objects related to a common goal/project

Data quantity
Data quality

### What we need
MySQL Server
MySQL Workbench - one of many tools that allow us to operate on MySQL databases

### Data types

Most used:
INT,
BIGINT,
VARCHAR,
TEXT,
BOOL,
DATE (default format YYYY-MM-DD),
TIME (default format HH24:mm:ss),
DATETIME (YYYY-MM-DD HH24:mm:ss)
ENUM

SQL particularities

VARCHAR - used to represent a limited set of characters 
TEXT - used to represent a large set of characters

BLOB - for datafiles, images, etc.

## COLUMN PROPERTIES

`UNIQUE` - does not allow for 2 or more same values on the same column
`NOT NULL` - does not allow NULL values on a column
`NULL` - by default - does allow NULL values on a column
`AUTO_INCREMENT` - has an internal index that auto increments for each new row
`DEFAULT` - allows us to have a default value for a column


let's say I have a `orders` table
id, code, order_status
```sql
CREATE TABLE orders (
    id int not null AUTO_INCREMENT,
    code varcahr not null,
    order_status varchar not null default 'NEW'
);

insert into orders (code) values ('C2342'),('D2343');
# this will work just fine and the records will have the order_status 'NEW'
```

## SQL - Structured Query Language

### Some syntax:

comments ( 2 types )
\# comment
-- comment

Strings:
'' - string
"" - string
`` - NOT A STRING, used to reference database objects like tables, columns, functions, views etc. safely

parts of an sql command are called **CLAUSES**
a very common clause that we will find in SQL is
**WHERE**
and it is used in multiple commands like:
SELECT
UPDATE
DELETE

and used mathematical operands to filder records.

mathematical operands:
'+'
'-'
'/'
'*'
'%'

conditional operators:
= - tests equality
'! =' - unequal
'<>' - also unequal but in Oracle
'>'
'<'
'>='
'<='

logical operators:
`AND`
`OR`
`NOT`

nullity operators:
nullity is NOT TESTED WITH =
`IS NULL`
`IS NOT NULL`

## Database Client tools || Database IDEs

MySQL Workbench
Navicat for Oracle, Navicat for MySQL
HeidiSQL
SQL Developer
DBeaver
Jetbrains Datagrip


## DDL - Data definition language

Allows us to:
Create structure
`CREATE`
`CREATE IF NOT EXISTS`
```sql
CREATE SCHEMA
CREATE TABLE
```
Alter/Modify structure
```sql
ALTER
ALTER TABLE ADD COLUMN
ALTER TABLE DROP COLUMN
ALTER TABLE MODIFY COLUMN
```
Drop structure
`DROP`
`DROP IF EXISTS`
`DROP TABLE IF EXISTS`
`DROP TABLE IF NOT EXISTS` - makes no sense to write this

extra:
```sql
DESCRIBE <table_name>;
```
returns the structure and properties of the table

## DML - data manipulation language

3 commands

INSERT - allows us to insert records into a table
UPDATE - allows us to modify records
DELETE - allows us to delete records

## INSERT
has multiple forms


```
Error Code: 1452. Cannot add or update a child row: a foreign key constraint fails (`sda_school_19`.`students`, CONSTRAINT `students_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`))
```

## UPDATE
particular syntax form
```sql
UPDATE <table> SET <column_name> = <new_value>;
```
the above statement will update ALL THE ROWS IN THE TABLE
```sql
update students set email = 'a@example.com';
```
this will update ALL THE ROWS IN THE TABLE
the use of this form is VERY RARE
most of the times we update, we only want to update a single record
this is where `WHERE` comes in handy

```sql
UPDATE <table> SET <column_name> = <new_value> WHERE <condition>;
```

## DELETE -- used to delete AN ENTIRE ROW or ROWS

```sql
-- the most common syntax
DELETE FROM <table_name> WHERE <condition>;
```
**BE VERY CAREFUL**
we can write delete statements without the WHERE condition and they might run, but probably we don't want that.

example:
we want to disable a user but we are in a hurry
```sql
UPDATE user set active = 0; -- this will disable ALL users
-- so by default the above is not permitted in certain editors
UPDATE user set active = 0 where id = 34535; -- this will update ONLY ONE user
```


## CRUD

# CREATE - INSERT
# READ - SELECT
# UPDATE - UPDATE
# DELETE - DELETE

### DQL - Data Query Language
```sql
SELECT
    <what?>
FROM 
    <where?>
WHERE
    <filter?>
GROUP BY
    <what?>
HAVING
    <filter?>
ORDER BY
    <ordering criteria?>
LIMIT
    <any pagination?>;
```
**THE ORDER OF THE CLAUSES IS MANDATORY**

## QUERY EXECUTION PLAN
the order of processing is different
1. FROM - we have to know where we NEED TO GET THE DATA FROM
2. WHERE - we filter the initial dataset
3. functions in SELECT are applied, the select outcome is starting to be prepared
4. GROUP BY - we transform the result if necessary
5. HAVING - we filter the grouped dataset
6. ORDER BY - we apply the ordering 
7. LIMIT - we select the slice we want to show
8. we get the desired columns and format the outcome as desired in the SELECT clause

in SELECT we define the output of the result

## ORM layer maps java classes to sql query
Object Relational Mapping
## DAO layer
Data Access Object

in WHERE we filter data

there is also the BETWEEN operator

all logical expressions in the WHERE clause can be applied by using `WHERE`
in the `UPDATE` and `DELETE` DML operations as well 

### HAVING
-- alows us to filter data post-grouping

## JOINS

**on query the input data will be structured by concatenating columns to the result format**
Table A (3 columns, A,B,C)
Table B (2 columns, D,E)
select * from tableA join tableB will result in:
(5 columns, A,B,C,D,E)

**INNER JOIN**
```sql
SELECT 
    * 
FROM 
    TableA
    INNER JOIN TableB 
    ON TableA.name = TableB.name;
```

**FULL OUTER JOIN**
_not available in MySQL_
```sql
SELECT 
    * 
FROM 
    TableA
    FULL OUTER JOIN TableB
    ON TableA.name = TableB.name;
```

**LEFT JOIN**
```sql
SELECT 
    * 
FROM 
    TableA
    LEFT JOIN TableB
    ON TableA.name = TableB.name
```

**RIGHT JOIN**
```sql
SELECT 
    * 
FROM 
    TableA
    RIGHT JOIN TableB
    ON TableA.name = TableB.name
```

**CROSS JOIN**
```sql
SELECT
    *
FROM
    TableA, TableB
```

**the output depends a lot on the starting table**

```sql
SELECT 
    *
FROM
    disciplines d
    LEFT JOIN teachers t
    ON d.id = t.discipline_id;

SELECT 
    *
FROM
    teachers t
    RIGHT JOIN disciplines d
    ON d.id = t.discipline_id;
```


## TRANSACTIONS
give us risk tolerance

acc1 = $1500
acc2 = $3000

op: transfer $1000 from acc1 to acc2 with a $10 tax


- save the initial state of the transaction
- validate acc1 can make transactions
- validate if we can tax acc1
- check if acc1 has $1000 + $10 ($1010)
- check if acc2 can receive money
- subtract $1000 from acc1
- subtract $10 from acc1
- update the state of the transaction
- ---------------- POWER OUTAGE
- add $10 to bank account
- add $1000 to acc2
- mark transaction as complete

```sql
BEGIN TRANSACTION;
-- do things
-- at the end of the transaction I need to do 1 of 2 things
COMMIT;
ROLLBACK;
END TRANSACTION;
```

transactions must be `ACID`
A - atomicity
C - consistency
I - isolation
D - durability

# methods of querying in Java applications
1. native query
2. hibernate query
3. criteria query
4. dynamic finders
5. simple get saves

