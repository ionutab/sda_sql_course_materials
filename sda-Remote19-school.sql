# very basic traditional school schema
-- you can run this sctipt entirely and it might work
CREATE SCHEMA `sda_school_19`;
use `sda_school_19`;
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
	`id` INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` VARCHAR(4) NOT NULL
);
CREATE TABLE `disciplines` (
	`id` INT AUTO_INCREMENT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
);
CREATE TABLE `students` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `first_name` VARCHAR(255) NOT NULL,
    `last_name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255),
    `telephone` VARCHAR(12),
    `date_of_birth` DATE NOT NULL,
    `class_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`class_id`)
        REFERENCES `classes` (`id`)
);
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `first_name` VARCHAR(255) NOT NULL,
    `last_name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255),
    `discipline_id` INT NOT NULL UNIQUE,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`discipline_id`)
        REFERENCES `disciplines` (`id`)
);
CREATE TABLE `classes_x_disciplines` (
	`class_id` INT NOT NULL,
    `discipline_id` INT NOT NULL,
    PRIMARY KEY(`class_id`, `discipline_id`),
    FOREIGN KEY (`class_id`)
        REFERENCES `classes` (`id`),
    FOREIGN KEY (`discipline_id`)
        REFERENCES `disciplines` (`id`)
);
drop table if exists `marks`;
CREATE TABLE IF NOT EXISTS `marks` (
    `id` INT AUTO_INCREMENT NOT NULL,
    `student_id` INT NOT NULL,
    `discipline_id` INT NOT NULL,
    `mark` TINYINT NOT NULL,
    `date` DATETIME NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`student_id`)
        REFERENCES `students` (`id`),
    FOREIGN KEY (`discipline_id`)
        REFERENCES `disciplines` (`id`)
);


# SOME SELECTS
select * from classes;
select * from students;
## DML
# INSERT
# form one - add a set of values that FIT THE TABLE STRUCTURE
INSERT INTO classes VALUES (1, '9A');
-- 1 will be the id
-- 9A will be the name
# we need to fit the table structure or the insert will not work
-- INSERT INTO classes VALUES ('9B', 2); - this goes into error becase 9B cannot go to the id col
# form two - add multiple sets of values at the same time
INSERT INTO classes VALUES (2, '9B'), (3, '10A'),(4, '10B');
# form three - define the columns on insert
INSERT INTO students (id, first_name, last_name, date_of_birth, class_id)
VALUES (1, 'Alexandru', 'A', '1988-01-01', 2);
# form four - change the insert statement table structure
-- the VALUES clause must respect the columns and the order of the columns defined in
-- the INTO clause
INSERT INTO students (id, class_id, first_name, last_name, date_of_birth)
VALUES (2, 4, 'Bianca', 'B', '1988-05-05');
INSERT INTO students (first_name, last_name, email, telephone, date_of_birth, class_id)
VALUES ('Cezar', 'C', 'cezar@gmail.com', '1234', '1987-09-05', 3),
('Daniel', 'D', 'daniel@gmail.com', '6542', '1989-08-15', 4),
('Elena', 'E', 'elena@yahoo.com', '3421', '1991-11-05', 1),
('Flavia', 'F', 'flavia@yahoo.com', '5555', '1987-05-02', 2),
('George', 'G', 'george@gmail.com', '9992', '1985-02-11', 3),
('Horatiu', 'H', 'horatiu@gmail.com', '9393', '1987-11-25', 2);
-- important: value sets are grouped using () and separated by ,
# adding some inserts into other tables
# DISCIPLINES
select * from disciplines;
insert into disciplines (name) 
VALUES 
('Math'), 
('History'), 
('Geography'), 
('Java'), 
('English');
SELECT * from teachers;
INSERT INTO teachers (first_name, last_name, email, discipline_id)
values 
('Herodot', 'H', 'herodot@yahoo.com', 2),
('Brancusi', 'B', 'brancusi@yahoo.com', 4),
('Pitagora', 'P', 'pitagora@gmail.com', 1),
('Emilia', 'Earhart', 'emilia@gmail.com', 3),
('Arthur', 'Schopenhauer', 'schopenhauer@gmail.com', 5);
### UPDATE
select * from students;
# we usually filter the rows we want to update by the id columns 
# we use set to provide new values
UPDATE students SET last_name = 'Popescu' WHERE id = 1;
# we can update multiple values
UPDATE students SET last_name = 'Marinescu',  telephone = '3453' WHERE id = 2;
# we can update multiple rows - when we don't filter by id
UPDATE students SET class_id = 1 WHERE date_of_birth >= '1988-01-01';
# we can compare even the values we will be overriding
UPDATE students set last_name = 'Flinstone' where last_name = 'F';
# we can set values to NULL
UPDATE students set email = null where email = 'elena@yahoo.com';
## DELETE - used to delete ROWS
delete from students where id = 8;
select * from teachers;
select * from disciplines;
-- delete from discipline where id = 4; will not work since discipline with id 4 is used in teachers









