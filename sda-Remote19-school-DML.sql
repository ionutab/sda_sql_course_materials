# SOME SELECTS
select * from classes;
select * from students;
## DML
# INSERT
# form one - add a set of values that FIT THE TABLE STRUCTURE
INSERT INTO classes VALUES (1, '9A');
-- 1 will be the id
-- 9A will be the name
# we need to fit the table structure or the insert will not work
-- INSERT INTO classes VALUES ('9B', 2); - this goes into error becase 9B cannot go to the id col
# form two - add multiple sets of values at the same time
INSERT INTO classes VALUES (2, '9B'), (3, '10A'),(4, '10B');
# form three - define the columns on insert
INSERT INTO students (id, first_name, last_name, date_of_birth, class_id)
VALUES (1, 'Alexandru', 'A', '1988-01-01', 2);
# form four - change the insert statement table structure
-- the VALUES clause must respect the columns and the order of the columns defined in
-- the INTO clause
INSERT INTO students (id, class_id, first_name, last_name, date_of_birth)
VALUES (2, 4, 'Bianca', 'B', '1988-05-05');
INSERT INTO students (first_name, last_name, email, telephone, date_of_birth, class_id)
VALUES ('Cezar', 'C', 'cezar@gmail.com', '1234', '1987-09-05', 3),
('Daniel', 'D', 'daniel@gmail.com', '6542', '1989-08-15', 4),
('Elena', 'E', 'elena@yahoo.com', '3421', '1991-11-05', 1),
('Flavia', 'F', 'flavia@yahoo.com', '5555', '1987-05-02', 2),
('George', 'G', 'george@gmail.com', '9992', '1985-02-11', 3),
('Horatiu', 'H', 'horatiu@gmail.com', '9393', '1987-11-25', 2);
-- important: value sets are grouped using () and separated by ,
# adding some inserts into other tables
# DISCIPLINES
select * from disciplines;
insert into disciplines (name) 
VALUES 
('Math'), 
('History'), 
('Geography'), 
('Java'), 
('English');
SELECT * from teachers;
INSERT INTO teachers (first_name, last_name, email, discipline_id)
values 
('Herodot', 'H', 'herodot@yahoo.com', 2),
('Brancusi', 'B', 'brancusi@yahoo.com', 4),
('Pitagora', 'P', 'pitagora@gmail.com', 1),
('Emilia', 'Earhart', 'emilia@gmail.com', 3),
('Arthur', 'Schopenhauer', 'schopenhauer@gmail.com', 5);
### UPDATE
select * from students;
# we usually filter the rows we want to update by the id columns 
# we use set to provide new values
UPDATE students SET last_name = 'Popescu' WHERE id = 1;
# we can update multiple values
UPDATE students SET last_name = 'Marinescu',  telephone = '3453' WHERE id = 2;
# we can update multiple rows - when we don't filter by id
UPDATE students SET class_id = 1 WHERE date_of_birth >= '1988-01-01';
# we can compare even the values we will be overriding
UPDATE students set last_name = 'Flinstone' where last_name = 'F';
# we can set values to NULL
UPDATE students set email = null where email = 'elena@yahoo.com';
## DELETE - used to delete ROWS
delete from students where id = 8;
select * from students;
select * from teachers;
select * from disciplines;
-- delete from discipline where id = 4; will not work since discipline with id 4 is used in teachers
-- delete from marks;
select * from students where id = 3;
# adding data into classes x disciplines
insert into classes_x_disciplines 
VALUES 
	(1, 1), (2, 1), (2, 2), (3, 3), (3, 1), (4, 1), (4, 5);
insert into classes_x_disciplines 
VALUES 
	(1, 3), (3, 4), (4, 2), (3, 5), (2, 5), (4, 3), (1, 5);

INSERT INTO students (first_name, last_name, email, telephone, date_of_birth, class_id)
VALUES ('Maria', 'Mihalache', 'maria@gmail.com', '234245', '1987-11-15', 4);

insert into marks 
(student_id, discipline_id, mark, date)
VALUES
(1, 1, CEIL(rand() * 10), '2021-05-06'),
(1, 1, CEIL(rand() * 10), '2021-05-06'),
(1, 2, CEIL(rand() * 10), '2021-05-06'),
(1, 2, CEIL(rand() * 10), '2021-05-26'),
(1, 4, CEIL(rand() * 10), '2021-05-26'),
(1, 3, CEIL(rand() * 10), '2021-05-26'),
(1, 3, CEIL(rand() * 10), '2021-05-06'),
(1, 5, CEIL(rand() * 10), '2021-05-06'),
(1, 5, CEIL(rand() * 10), '2021-05-16'),
(2, 5, CEIL(rand() * 10), '2021-05-16'),
(5, 1, CEIL(rand() * 10), '2021-06-16'),
(2, 1, CEIL(rand() * 10), '2021-05-16'),
(3, 2, CEIL(rand() * 10), '2021-05-06'),
(3, 2, CEIL(rand() * 10), '2021-05-16'),
(2, 4, CEIL(rand() * 10), '2021-05-06'),
(3, 3, CEIL(rand() * 10), '2021-05-16'),
(3, 3, CEIL(rand() * 10), '2021-05-26'),
(3, 5, CEIL(rand() * 10), '2021-05-26'),
(2, 5, CEIL(rand() * 10), '2021-05-26'),
(2, 5, CEIL(rand() * 10), '2021-05-26'),
(7, 5, CEIL(rand() * 10), '2021-05-06'),
(7, 1, CEIL(rand() * 10), '2021-05-06'),
(5, 1, CEIL(rand() * 10), '2021-05-06'),
(5, 2, CEIL(rand() * 10), '2021-05-06'),
(7, 2, CEIL(rand() * 10), '2021-05-06'),
(7, 4, CEIL(rand() * 10), '2021-05-06'),
(7, 3, CEIL(rand() * 10), '2021-06-06'),
(8, 3, CEIL(rand() * 10), '2021-06-06'),
(7, 5, CEIL(rand() * 10), '2021-06-06'),
(8, 5, CEIL(rand() * 10), '2021-06-06'),
(8, 5, CEIL(rand() * 10), '2021-06-06');

insert into marks 
(student_id, discipline_id, mark, date)
VALUES
(2, 1, CEIL(rand() * 10), '2021-05-06'),
(1, 1, CEIL(rand() * 10), '2021-05-16'),
(1, 3, CEIL(rand() * 10), '2021-05-06'),
(1, 5, CEIL(rand() * 10), '2021-05-22'),
(1, 4, CEIL(rand() * 10), '2021-05-22'),
(1, 3, CEIL(rand() * 10), '2021-05-22'),
(4, 1, CEIL(rand() * 10), '2021-05-06'),
(4, 5, CEIL(rand() * 10), '2021-05-06'),
(4, 5, CEIL(rand() * 10), '2021-05-16'),
(2, 4, CEIL(rand() * 10), '2021-05-16'),
(5, 1, CEIL(rand() * 10), '2021-06-22'),
(2, 1, CEIL(rand() * 10), '2021-05-22'),
(3, 2, CEIL(rand() * 10), '2021-05-22'),
(3, 2, CEIL(rand() * 10), '2021-05-16'),
(2, 5, CEIL(rand() * 10), '2021-05-06'),
(1, 3, CEIL(rand() * 10), '2021-05-16'),
(1, 3, CEIL(rand() * 10), '2021-05-26'),
(3, 5, CEIL(rand() * 10), '2021-05-21'),
(2, 5, CEIL(rand() * 10), '2021-05-21'),
(2, 5, CEIL(rand() * 10), '2021-05-21'),
(6, 5, CEIL(rand() * 10), '2021-05-21'),
(6, 1, CEIL(rand() * 10), '2021-05-06'),
(6, 1, CEIL(rand() * 10), '2021-05-06'),
(6, 2, CEIL(rand() * 10), '2021-05-06'),
(7, 1, CEIL(rand() * 10), '2021-05-06'),
(7, 1, CEIL(rand() * 10), '2021-05-06'),
(7, 1, CEIL(rand() * 10), '2021-06-06'),
(8, 3, CEIL(rand() * 10), '2021-06-21'),
(7, 5, CEIL(rand() * 10), '2021-06-21'),
(8, 5, CEIL(rand() * 10), '2021-06-21'),
(8, 5, CEIL(rand() * 10), '2021-04-06');

INSERT INTO students (first_name, last_name, email, telephone, date_of_birth, class_id)
VALUES ('Adrian', 'Mihalache', 'adrian@gmail.com', '2334211', '1989-09-23', 2);
INSERT INTO students (first_name, last_name, email, telephone, date_of_birth, class_id)
VALUES ('Damien', 'Schopenhauer', 'damien@gmail.com', '23142', '1992-01-23', 3);
