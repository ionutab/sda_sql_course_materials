##DQL
SELECT 
    *
FROM
    students;
# * = all
# we can filter, or TRANSFORM the format of the result
SELECT 
    first_name, last_name
FROM
    students;
SELECT 
    last_name, first_name
FROM
    students;
# we can call internal functions as well
SELECT now();
# returns the current timestamp
SELECT rand();
# returns a random number between 0 and 1
SELECT rand() * 10;
# random between 0 and 9.99999
SELECT FLOOR(rand() * 10);
# random between 0 and 9
SELECT FLOOR(rand() * 10) + 1;
# random between 1 and 10
# opposite of FLOOR is CEIL
SELECT CEIL(rand() * 10);
# there are string functions as well
select concat(first_name, last_name) from students;
select concat(first_name, ' ', last_name) from students;
select concat(last_name, ' ', first_name) from students;
select concat(SUBSTRING(last_name, 1, 1), ' ', first_name) from students;
# first 1 represents the start position
# second 1 represent the number of characters to read from the start position
# we can have aliases
select concat(first_name, ' ', last_name) as full_name
from students;
select concat(first_name, ' ', last_name) as `full name`
from students;
# we can give aliases to better recognise what is displayed
SELECT 
    first_name as `first`, 
    last_name as `last`,
    email as `e`,
    telephone as `t`
FROM
    students;
SELECT 
    last_name as `last`,
    first_name as `first`, 
    email as `e`,
    telephone as `t`,
    id
FROM
    students;
# by default ALL queries are run on the currently selected schema
SELECT 
    last_name as `last`,
    first_name as `first`, 
    email as `e`,
    telephone as `t`,
    id
FROM
    students;
# we can specify the table from which the columns are selected in a more verbose wayd
SELECT 
    students.last_name as `last`,
    students.first_name as `first`, 
    students.email as `e`,
    students.telephone as `t`,
    students.id
FROM
    students;
# we can have aliases for tables as well
SELECT 
    s.last_name as `last`,
    s.first_name as `first`, 
    s.email as `e`,
    s.telephone as `t`,
    s.id
FROM
    students s;
# THE WHERE CLAUSE
# allows us to filter data
# allows us to describe a logical expression that will be evaluated for each row
# only the rows/records that satisfy the expression will be returned
SELECT 
    *
FROM
    students
WHERE
    id > 5;

select * from students;
SELECT 
    *
FROM
    students
WHERE
    class_id = 1 AND id > 2;
SELECT 
    *
FROM
    students
WHERE
    class_id = 1 OR id > 2;
select * from students where date_of_birth < now();
-- select * from students where class_id = ceil(rand() * 10); -- a new random is generated for each record in this case
select * from classes;
select * from disciplines;
select * from teachers;
select * from classes_x_disciplines;
select * from students where id != 4;
select * from students where email is null;
select * from students where email is NOT null;
select * from students where email is NOT null;
select * from students where last_name = 'G';
select * from students where telephone = '9992';
select * from students where telephone = 9992;
# we can compare dates
select * from students where date_of_birth > '1989-01-01';
select * from students where date_of_birth >= '1989-01-01';
# BETWEEN
select * from students where id between 2 and 5;
# attention the interval limits are INCLUDED
select * from students where date_of_birth between '1987-05-07' and '1989-09-09';
select * from students where last_name between 'C' and 'H';
select * from students where id = 5;
select * from students;
# another operarot is LIKE
# allows us to do pattern matching on strings
select * from students where last_name = 'E';
select * from students where last_name LIKE 'E';
# % replaces any number of any character
# searching for students that have a last name starting with M
select * from students where last_name LIKE 'M%';
# searching for students that have at last an 'a' in their first name
select * from students where first_name LIKE '%a%';
select * from students where first_name LIKE '%a%a%';
# _ - replaces only any one character
select * from students where first_name LIKE '%a_a%';
select * from students where first_name LIKE '%a__a%';
# let's say we want all students that have a gmail account
select * from students where email like '%@gmail.com';
select * from marks;
# AGGREGATE FUNCTIONS
# COUNT, MAX, MIN, SUM, AVG
select count(id) from students;
select max(id) from students;
select min(id) from students;
select sum(id) from students;
select avg(id) from students;
select sum(mark) from marks;
select avg(mark) from marks;
select min(mark) from marks;
select max(mark) from marks;
-- count only counts non-null values
SELECT 
    *
FROM
    students;
select count(email) from students;
select count(telephone) from students;
select count(last_name) from students; -- only tests the existence of non-null values on the specified column
select count(*) from students; -- evaluates the PK of the table\
# often used with count is DISTINCT
select count(last_name) from students; -- 9
select count(distinct last_name) from students; -- 8 since we have 2 students with the same last name

-- what happends when, for example I want to count the number of students in EACH CLASS
select count(*) from students where class_id = 1;
select count(*) from students where class_id = 2;
select count(*) from students where class_id = 3;
select count(*) from students where class_id = 4;

select class_id, count(*) from students; -- this is useless
-- this will return the class_id of the first record and the total number of students
-- there is no context in which this non-corelation of information could be valid

## GROUP BY to the rescue
SELECT 
    class_id, COUNT(id)
FROM
    students
GROUP BY class_id; 
# let's determine the average for each student
SELECT 
    student_id, AVG(mark)
FROM
    marks
GROUP BY student_id;
# let's determine the best mark for each student
SELECT 
    student_id, MAX(mark)
FROM
    marks
GROUP BY student_id;
# let's determine the best mark for each student at each discipline
SELECT 
    student_id, discipline_id, MAX(mark)
FROM
    marks
GROUP BY student_id, discipline_id;
# let's say I want to determine the average of marks for each month
select * FROM MARKS;
# there are some calendar functions
select *, YEAR(date) from marks;
select *, MONTH(date) from marks;
select *, DAY(date) from marks;
select *, dayofmonth(date) from marks;
SELECT 
    YEAR(date), MONTH(date), AVG(mark)
FROM
    marks
GROUP BY YEAR(date) , MONTH(date);
SELECT 
    YEAR(date) as `year`, MONTH(date) as `month`, AVG(mark)
FROM
    marks
GROUP BY `year`, `month`;
SELECT 
    CONCAT(YEAR(date), '-', MONTH(date)) as `yearmonth`, AVG(mark)
FROM
    marks
GROUP BY `yearmonth`;
-- atention, non-aggregate functions in the SELECT clause are called for each record individually
# HAVING
select * from marks;
# if I want to see all marks bigger than 5
select * from marks WHERE mark > 5;
# I want to see all AVERAGES bigger than 5
SELECT 
    student_id, AVG(mark)
FROM
    marks
WHERE
    mark > 5
GROUP BY student_id; -- this is bad, it determines the average of marks bigger than 5 for each student
SELECT 
    student_id, AVG(mark) AS `student_avg`
FROM
    marks
GROUP BY student_id
HAVING `student_avg` > 5; -- this is good
# HAVING allows us to filter the results AFTER THE GROUP BY HAS BEEN MADE
SELECT 
    student_id, AVG(mark) AS `some_avg`
FROM
    marks
WHERE
    mark > 3
GROUP BY student_id
HAVING some_avg > 7;
# will show the students that have an (average of (marks > 3) > 7)
SELECT 
    student_id, AVG(mark) AS `math_avg`
FROM
    marks
WHERE
    discipline_id = 1
GROUP BY student_id
HAVING math_avg > 7;
# will show the students that have an average > 7 at math
## ORDER BY
select * from students;
select * from students order by last_name; -- by default the ordering is ASC
## 2 key words: ASC, DESC
select * from students order by last_name ASC;
select * from students order by last_name DESC;
select * from students order by last_name ASC, first_name ASC;
select last_name, first_name from students order by last_name ASC, first_name ASC;
select last_name, first_name from students order by last_name ASC, first_name DESC;
SELECT 
    student_id, AVG(mark) AS `student_avg`
FROM
    marks
GROUP BY student_id
HAVING `student_avg` > 5
ORDER BY `student_avg` desc;
## LIMIT
-- allows us to paginate results
select * from marks;
select * from marks order by date asc;
select * from marks order by date asc limit 0, 5;
-- first param is offset
-- second param is page size
select * from marks order by date asc limit 0, 5; -- first page
select * from marks order by date asc limit 5, 5; -- second page
select * from marks order by date asc limit 10, 5; -- third page
select * from marks order by date asc limit 0, 10; -- first page
select * from marks order by date asc limit 10, 10; -- second page
select * from marks order by date asc limit 20, 10; -- third page
select * from marks order by date asc limit 1140, 60; -- 20th page when 60 products per page
## JOINS
-- allow us to create a initial query dataset from two or more tables
-- used in FROM clause
-- multiple types of joins
## CROSS JOIN - CARTESIAN PRODUCT
select * from classes;
select * from disciplines;
select * from classes, disciplines;
-- kind of useless
## INNER JOIN
select * from students;
select * from classes;
SELECT 
    *
FROM
    students s
    INNER JOIN classes c 
    ON s.class_id = c.id;
# THIS IS GOOD - makes sense since students are corelated to classes
SELECT  
    s.id,
    s.first_name,
    s.last_name,
    c.name
FROM
    students s
    INNER JOIN classes c 
    ON s.class_id = c.id;
# this is bad - students are not directly corelated to disciplines
SELECT  
    s.id,
    s.first_name,
    s.last_name,
    d.name
FROM
    students s
    INNER JOIN disciplines d 
    ON s.class_id = d.id;
select * from teachers;
# I am the one who has to determine if the join makes any sense
# the following 2 examples are just to show this 
SELECT 
    *
FROM
    teachers t
        INNER JOIN
    students s ON t.last_name = s.last_name;
SELECT 
    *
FROM
    teachers t
        LEFT JOIN
    students s ON t.last_name = s.last_name;
# LEFT VS RIGHT JOIN
# I want to see the teacher for each discipline
SELECT 
    *
FROM
    disciplines d
    LEFT JOIN teachers t
    ON d.id = t.discipline_id;
SELECT 
    *
FROM
    disciplines d
    RIGHT JOIN teachers t
    ON d.id = t.discipline_id;
SELECT 
    *
FROM
    teachers t
    RIGHT JOIN disciplines d
    ON d.id = t.discipline_id;
## viewing the student names and discipline names for each mark
SELECT 
    -- s.first_name, s.last_name, d.name, m.mark
    * 
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
        INNER JOIN
    disciplines d ON m.discipline_id = d.id
;
## viewing the student names , class names, and discipline names for each mark
SELECT 
    s.first_name, s.last_name, c.name, d.name, m.mark
    -- * 
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
        INNER JOIN
    disciplines d ON m.discipline_id = d.id
		INNER JOIN
	classes c on s.class_id = c.id
;
## student, class, discipline, mean - method with joins
SELECT 
    s.first_name,
    s.last_name,
    c.name AS 'class_name',
    d.name AS 'discipline_name',
    AVG(m.mark)
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
        INNER JOIN
    disciplines d ON m.discipline_id = d.id
        INNER JOIN
    classes c ON s.class_id = c.id
GROUP BY s.first_name , s.last_name , c.name , d.name
;

## student, class, discipline, mean - method with joins - showing only grades < 5
SELECT 
    s.first_name,
    s.last_name,
    c.name AS 'class_name',
    d.name AS 'discipline_name',
    AVG(m.mark) AS `average_per_discipline`
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
        INNER JOIN
    disciplines d ON m.discipline_id = d.id
        INNER JOIN
    classes c ON s.class_id = c.id
GROUP BY s.first_name , s.last_name , c.name , d.name
HAVING average_per_discipline < 5
;
#SUBQUERIES
# the average for each student
SELECT 
    s.id,
    s.first_name,
    s.last_name,
    AVG(m.mark) AS `average`
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
GROUP BY s.id , s.first_name , s.last_name;
# the average for each class
SELECT 
    c.id, c.name, AVG(mark)
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
        INNER JOIN
    classes c ON s.class_id = c.id
GROUP BY c.id;
#simple SUBQUERY 
SELECT 
    *
FROM
    (SELECT 
        c.id, c.name, AVG(mark)
    FROM
        marks m
    INNER JOIN students s ON m.student_id = s.id
    INNER JOIN classes c ON s.class_id = c.id
    GROUP BY c.id) `averages_per_class`;


# show me the students having an average above their classes average
# method 1 , subquery in select - most of the time not necessary and costly
SELECT 
    s.id,
    s.first_name,
    s.last_name,
    s.class_id,
    AVG(m.mark) AS `average`,
    (SELECT 
            AVG(m1.mark)
        FROM
            marks m1
                INNER JOIN
            students s1 ON m1.student_id = s1.id
        WHERE
            s1.class_id = s.class_id) AS `class_average`
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
GROUP BY s.id , s.first_name , s.last_name
-- HAVING `average` > `class_average`
;
# second method - subquery in FROM
# efficient, executes only once
SELECT 
    s.id, s.first_name, s.last_name, s.class_id, ac.class_average, AVG(m.mark) AS `average`
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
        INNER JOIN
    (SELECT 
        c.id AS class_id, c.name, AVG(mark) as `class_average`
    FROM
        marks m
    INNER JOIN students s ON m.student_id = s.id
    INNER JOIN classes c ON s.class_id = c.id
    GROUP BY c.id) ac ON ac.class_id = s.class_id
GROUP BY s.id , s.first_name , s.last_name
-- HAVING `average` > `class_average`
;
-- subqueries are often used in EXISTS clauses
-- exists is used in WHERE to test if something exists or not
# I want to see all students that have at least one mark < 2
select * from students where class_id = 1;

SELECT 
    m.mark
FROM
    marks m
WHERE
    m.student_id = 9 AND m.mark < 2;
# subquery in exists
SELECT 
    s.id, s.first_name, s.last_name
FROM
    students s
WHERE
    EXISTS( SELECT 
            m.mark
        FROM
            marks m
        WHERE
            m.student_id = s.id AND m.mark < 2);
            
SELECT 
    s.id, s.first_name, s.last_name
FROM
    students s
WHERE
    NOT EXISTS( SELECT 
            m.mark
        FROM
            marks m
        WHERE
            m.student_id = s.id AND m.mark < 2);
# same problem resolved with joins
SELECT 
    DISTINCT s.id, s.first_name, s.last_name
FROM
    marks m
        LEFT JOIN
    students s ON m.student_id = s.id
    where m.mark < 2;
# VIEW - a view is a query stored in the memory of the database that you can reuse
select * from class_averages;

SELECT 
    s.id,
    s.first_name,
    s.last_name,
    s.class_id,
    ac.class_average,
    AVG(m.mark) AS `average`
FROM
    marks m
        INNER JOIN
    students s ON m.student_id = s.id
        INNER JOIN
    class_averages ac ON ac.class_id = s.class_id
GROUP BY s.id , s.first_name , s.last_name
;

create view `class_averages` as SELECT 
        c.id AS class_id, c.name, AVG(mark) as `class_average`
    FROM
        marks m
    INNER JOIN students s ON m.student_id = s.id
    INNER JOIN classes c ON s.class_id = c.id
    GROUP BY c.id;
