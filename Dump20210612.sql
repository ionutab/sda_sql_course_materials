CREATE DATABASE  IF NOT EXISTS `sda_school_19` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `sda_school_19`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: sda_school_19
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `class_averages`
--

DROP TABLE IF EXISTS `class_averages`;
/*!50001 DROP VIEW IF EXISTS `class_averages`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `class_averages` AS SELECT 
 1 AS `class_id`,
 1 AS `name`,
 1 AS `class_average`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (1,'9A'),(2,'9B'),(3,'10A'),(4,'10B');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes_x_disciplines`
--

DROP TABLE IF EXISTS `classes_x_disciplines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classes_x_disciplines` (
  `class_id` int NOT NULL,
  `discipline_id` int NOT NULL,
  PRIMARY KEY (`class_id`,`discipline_id`),
  KEY `discipline_id` (`discipline_id`),
  CONSTRAINT `classes_x_disciplines_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  CONSTRAINT `classes_x_disciplines_ibfk_2` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes_x_disciplines`
--

LOCK TABLES `classes_x_disciplines` WRITE;
/*!40000 ALTER TABLE `classes_x_disciplines` DISABLE KEYS */;
INSERT INTO `classes_x_disciplines` VALUES (1,1),(2,1),(3,1),(4,1),(2,2),(4,2),(1,3),(3,3),(4,3),(3,4),(1,5),(2,5),(3,5),(4,5);
/*!40000 ALTER TABLE `classes_x_disciplines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplines`
--

DROP TABLE IF EXISTS `disciplines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `disciplines` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplines`
--

LOCK TABLES `disciplines` WRITE;
/*!40000 ALTER TABLE `disciplines` DISABLE KEYS */;
INSERT INTO `disciplines` VALUES (1,'Math'),(2,'History'),(3,'Geography'),(4,'Java'),(5,'English');
/*!40000 ALTER TABLE `disciplines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marks`
--

DROP TABLE IF EXISTS `marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `discipline_id` int NOT NULL,
  `mark` tinyint NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `discipline_id` (`discipline_id`),
  CONSTRAINT `marks_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  CONSTRAINT `marks_ibfk_2` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks`
--

LOCK TABLES `marks` WRITE;
/*!40000 ALTER TABLE `marks` DISABLE KEYS */;
INSERT INTO `marks` VALUES (1,1,1,10,'2021-05-06 00:00:00'),(2,1,1,2,'2021-05-06 00:00:00'),(3,1,2,10,'2021-05-06 00:00:00'),(4,1,2,2,'2021-05-26 00:00:00'),(5,1,4,10,'2021-05-26 00:00:00'),(6,1,3,5,'2021-05-26 00:00:00'),(7,1,3,2,'2021-05-06 00:00:00'),(8,1,5,5,'2021-05-06 00:00:00'),(9,1,5,8,'2021-05-16 00:00:00'),(10,2,5,4,'2021-05-16 00:00:00'),(11,5,1,6,'2021-06-16 00:00:00'),(12,2,1,7,'2021-05-16 00:00:00'),(13,3,2,8,'2021-05-06 00:00:00'),(14,3,2,7,'2021-05-16 00:00:00'),(15,2,4,10,'2021-05-06 00:00:00'),(16,3,3,9,'2021-05-16 00:00:00'),(17,3,3,4,'2021-05-26 00:00:00'),(18,3,5,2,'2021-05-26 00:00:00'),(19,2,5,10,'2021-05-26 00:00:00'),(20,2,5,3,'2021-05-26 00:00:00'),(21,7,5,5,'2021-05-06 00:00:00'),(22,7,1,6,'2021-05-06 00:00:00'),(23,5,1,2,'2021-05-06 00:00:00'),(24,5,2,2,'2021-05-06 00:00:00'),(25,7,2,4,'2021-05-06 00:00:00'),(26,7,4,4,'2021-05-06 00:00:00'),(27,7,3,9,'2021-06-06 00:00:00'),(28,8,3,9,'2021-06-06 00:00:00'),(29,7,5,1,'2021-06-06 00:00:00'),(30,8,5,4,'2021-06-06 00:00:00'),(31,8,5,9,'2021-06-06 00:00:00'),(32,2,1,3,'2021-05-06 00:00:00'),(33,1,1,5,'2021-05-16 00:00:00'),(34,1,3,8,'2021-05-06 00:00:00'),(35,1,5,3,'2021-05-22 00:00:00'),(36,1,4,1,'2021-05-22 00:00:00'),(37,1,3,8,'2021-05-22 00:00:00'),(38,4,1,3,'2021-05-06 00:00:00'),(39,4,5,1,'2021-05-06 00:00:00'),(40,4,5,6,'2021-05-16 00:00:00'),(41,2,4,5,'2021-05-16 00:00:00'),(42,5,1,7,'2021-06-22 00:00:00'),(43,2,1,10,'2021-05-22 00:00:00'),(44,3,2,7,'2021-05-22 00:00:00'),(45,3,2,4,'2021-05-16 00:00:00'),(46,2,5,1,'2021-05-06 00:00:00'),(47,1,3,3,'2021-05-16 00:00:00'),(48,1,3,2,'2021-05-26 00:00:00'),(49,3,5,1,'2021-05-21 00:00:00'),(50,2,5,8,'2021-05-21 00:00:00'),(51,2,5,7,'2021-05-21 00:00:00'),(52,6,5,9,'2021-05-21 00:00:00'),(53,6,1,4,'2021-05-06 00:00:00'),(54,6,1,2,'2021-05-06 00:00:00'),(55,6,2,9,'2021-05-06 00:00:00'),(56,7,1,7,'2021-05-06 00:00:00'),(57,7,1,6,'2021-05-06 00:00:00'),(58,7,1,10,'2021-06-06 00:00:00'),(59,8,3,2,'2021-06-21 00:00:00'),(60,7,5,1,'2021-06-21 00:00:00'),(61,8,5,6,'2021-06-21 00:00:00'),(62,8,5,6,'2021-04-06 00:00:00');
/*!40000 ALTER TABLE `marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telephone` varchar(12) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `class_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Alexandru','Popescu',NULL,NULL,'1988-01-01',1),(2,'Bianca','Marinescu',NULL,'3453','1988-05-05',1),(3,'Cezar','C','cezar@gmail.com','1234','1987-09-05',3),(4,'Daniel','D','daniel@gmail.com','6542','1989-08-15',1),(5,'Elena','E',NULL,'3421','1991-11-05',1),(6,'Flavia','Flinstone','flavia@yahoo.com','5555','1987-05-02',2),(7,'George','G','george@gmail.com','9992','1985-02-11',3),(8,'Maria','Mihalache','maria@gmail.com','234245','1987-11-15',4),(17,'Adrian','Mihalache','adrian@gmail.com','2334211','1989-09-23',2),(18,'Damien','Schopenhauer','damien@gmail.com','23142','1992-01-23',3);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teachers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `discipline_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `discipline_id` (`discipline_id`),
  CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES (1,'Herodot','H','herodot@yahoo.com',2),(2,'Brancusi','B','brancusi@yahoo.com',4),(3,'Pitagora','P','pitagora@gmail.com',1),(4,'Emilia','Earhart','emilia@gmail.com',3),(5,'Arthur','Schopenhauer','schopenhauer@gmail.com',5);
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'sda_school_19'
--

--
-- Dumping routines for database 'sda_school_19'
--

--
-- Final view structure for view `class_averages`
--

/*!50001 DROP VIEW IF EXISTS `class_averages`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `class_averages` AS select `c`.`id` AS `class_id`,`c`.`name` AS `name`,avg(`m`.`mark`) AS `class_average` from ((`marks` `m` join `students` `s` on((`m`.`student_id` = `s`.`id`))) join `classes` `c` on((`s`.`class_id` = `c`.`id`))) group by `c`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-12 11:39:31
